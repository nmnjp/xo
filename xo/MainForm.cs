﻿
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace xo
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		
		int [][] matrica = new int[3][];
		int koIgra;
		public MainForm()
		{
			
			InitializeComponent();
			for (int i = 0 ; i < 3 ; i++){
				matrica[i] = new int[3] {0,0,0};
			}
			matrica[2][2] = 1;
			koIgra = 0;
		}
		void MainFormLoad(object sender, EventArgs e)
		{
	
		}
		void Click(object sender, EventArgs e)
		{
			Button b = (Button) sender;
			int x = int.Parse(b.Name.Substring(1,1));
			
			
			if(b.Name == "b1") matrica [0][0] = koIgra%2 + 1;
			if(b.Name == "b2") matrica [0][1] = koIgra%2 + 1;
			if(b.Name == "b3") matrica [0][2] = koIgra%2 + 1;
			
			if(koIgra % 2 == 0) b.Text = "X";
			else b.Text = "O";
			koIgra ++;
			
		}
		
	}
}
